I am stumpped
=============

Requiring a library that makes use of `Bundler.setup` or `Bundler.require` can
sometimes prevent gems from being loaded.

In this case the code fails with `LoadError no such file to load -- active_support`.


Reproduce
---------

``` bash
cd your-app
ruby app.rb
```

If you remove the call to `Bundler.setup` in `some-lib/lib/wtf.rb` then
everything functions as expected.

