File.expand_path('../',  __FILE__).tap { |p| $LOAD_PATH.unshift(p) unless $LOAD_PATH.include?(p) }

require 'pp'
require 'bundler'

pp $LOAD_PATH
pp Bundler.bundle_path
Bundler.setup

require 'active_support'

